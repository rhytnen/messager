﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Timers;
using System.Collections.Concurrent;

using ConcourseCommon;

namespace ConcourseNet
{

    /// <summary>
    /// Connection managers aggregate connections from a particular source (like a TcpListener) and pass messages between the connections.
    /// Connection managers also provide interest to other connection managers so that various sources have some way to pass messages.
    /// </summary>
    public class ConnectionManager
    {
        #region fields

        /// <summary>
        /// A list of connections that are currently being managed.
        /// </summary>
        ConcurrentDictionary<int, Connection> connections = new ConcurrentDictionary<int, Connection>();    //key is connection ID
        
        /// <summary>
        /// A list of messages that this manager is interested in.
        /// </summary>
        List<int> registeredIds = new List<int>();

        /// <summary>
        /// A dictionary (map) from a connection to a list of ids that it is interested in.
        /// </summary>
        ConcurrentDictionary<Connection, List<int>> clientRegistrationMap = new ConcurrentDictionary<Connection, List<int>>();

        #endregion

        #region properties

        public IEnumerable<int> RegisteredIds
        {
            get { return registeredIds; }
        }

        #endregion

        #region membership functions

        /// <summary>
        /// Add a connection to a manager.
        /// </summary>
        /// <param name="coll">The manager to take the new connection.</param>
        /// <param name="elem">The connection to add.</param>
        /// <returns></returns>
        public static ConnectionManager operator +(ConnectionManager coll, Connection elem)
        {
            coll.connections.TryAdd(elem.ConnectionID, elem);
            elem.MessageReceivedEvent += new EventHandler<ConnectionEventArgs>(coll.OnMsgReceived);
            return coll;
        }
        
        /// <summary>
        /// Remove a connection from a managers list of connections.
        /// </summary>
        /// <param name="coll">The manager containing the connection.</param>
        /// <param name="elem">The connection to prune.</param>
        /// <returns></returns>
        public static ConnectionManager operator -(ConnectionManager coll, Connection elem)
        {
            Connection connection = null;
            coll.connections.TryRemove(elem.ConnectionID, out connection);
            elem.MessageReceivedEvent -= new EventHandler<ConnectionEventArgs>(coll.OnMsgReceived);
            return coll;
        }

        #endregion


        /// <summary>
        /// Close down all connections.
        /// </summary>
        public void CloseConnections()
        {
            foreach (KeyValuePair<int, Connection> kvp in connections)
            {
                if (kvp.Value != null)
                {
                    kvp.Value.Close();
                }
            }
        }

        #region message handler functions

        /// <summary>
        /// This function is called when a client registers for an id.
        /// </summary>
        /// <param name="id">The id registered.</param>
        /// <param name="con">The conenction registering the id.</param>
        void RegisterClientId(int id, Connection con)
        {
            if (clientRegistrationMap.ContainsKey(con))
            {
                clientRegistrationMap[con].Add(id);
            }
            else
            {
                List<int> ids = new List<int>();
                ids.Add(id);
                clientRegistrationMap.TryAdd(con, ids);
            }
        }

        /// <summary>
        /// Remove this id from a connections list of registered ids.
        /// </summary>
        /// <param name="id">The id to remove from the list.</param>
        /// <param name="con">The connection from which to remove the id.</param>
        void DeregisterClientId(int id, Connection con)
        {
            List<int> value = null;
            if (clientRegistrationMap.TryGetValue(con, out value))
            {
                if (value != null)
                {
                    value.Remove(id);
                }
            }
        }

        /// <summary>
        /// This looks for connections setup as hubs (i.e. other routers) and registers / deregisters messages with them.
        /// </summary>
        /// <param name="con"></param>
        /// <param name="reg"></param>
        void ForwardRegistrationToHubs(Connection con, int reg)
        {
            byte[] message = BitConverter.GetBytes(reg);
            foreach (KeyValuePair<int, Connection> kvp in connections)
            {
                if (kvp.Value == null || kvp.Value == con)
                {
                    continue;
                }
                if (kvp.Value.IsHub == true)
                {
                    kvp.Value.WriteMessage(0, message);
                }
            }
        }

        /// <summary>
        /// This looks for connections setup as hubs (i.e. other routers) and registers / deregisters messages with them.
        /// </summary>
        /// <param name="con"></param>
        /// <param name="reg"></param>
        void ForwardDeregistrationToHubs(Connection con, int reg)
        {
            byte[] message = BitConverter.GetBytes(reg);
            foreach (KeyValuePair<int, Connection> kvp in connections)
            {
                if (kvp.Value == null || kvp.Value == con)
                {
                    continue;
                }
                if (kvp.Value.IsHub == true)
                {
                    kvp.Value.WriteMessage(-1, message);
                }
            }
        }

        /// <summary>
        /// When a message comes in on a connection, this is the function that will 
        /// handle it and pass it off to other interested connections.  A connection will not recieve
        /// it's own message.
        /// </summary>
        /// <param name="message">The message to handle.</param>
        /// <param name="id">The id on which a message was received.</param>
        /// <param name="con">The connection which sent this message.</param>
        void HandleMessage(byte[] m, int id, Connection con)
        {        
            switch (id)
            {
                case 0: // register                       
                    int reg = BitConverter.ToInt32(m, 0);
                    Console.WriteLine("Connection registering for " + reg);
                    RegisterClientId(reg, con);
                    if (con.IsHub == false)
                    {
                        ForwardRegistrationToHubs(con, reg);
                    }
                    break;
                case -1: // deregister
                    int dereg = System.BitConverter.ToInt32(m, 0);
                    Console.WriteLine("Connection deregistering for " + dereg);
                    DeregisterClientId(dereg, con);
                    // if the sender is a hub, you don't want them procing con.
                    if (con.IsHub == false)
                    {
                        ForwardDeregistrationToHubs(con, dereg);
                    }
                    break;
                case -2:  // name
                    con.Name = System.Text.ASCIIEncoding.UTF8.GetString(m);
                    Console.WriteLine("Connection naming itself {0}", con.Name);
                    break;
                case -3:
                    Console.WriteLine("Connection is a hub.");
                    con.IsHub = true;
                    break;
                default:
                    // Every interested client gets the message
                    List<Connection> disconnectedConnections = null;
                    foreach (KeyValuePair<Connection, List<int>> kvp in clientRegistrationMap)
                    {
                        if (kvp.Key.IsConnected() == false)
                        {
                            if (disconnectedConnections == null)
                            {
                                disconnectedConnections = new List<Connection>();
                            }
                            disconnectedConnections.Add(kvp.Key);
                            continue;
                        }

                        if (con == kvp.Key) // self
                            continue;

                        foreach (int mid in kvp.Value.ToList())
                        {
                            if (mid == id)
                            {
                                kvp.Key.WriteMessage(id, m);
                            }
                        }
                    }

                    if (disconnectedConnections!=null)
                    {
                        foreach (Connection connection in disconnectedConnections)
                        {
                            if (connection.IsConnected() == false)
                            {
                                List<int> idList = null;
                                clientRegistrationMap.TryRemove(connection, out idList);
                                if (connection.IsHub == false && idList != null)
                                {
                                    foreach (int msgID in idList)
                                    {
                                        ForwardDeregistrationToHubs(connection, msgID);
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        void OnMsgReceived(object sender, ConnectionEventArgs eventArgs)
        {
            if (eventArgs == null)
            {
                return;
            }

            try
            {
                HandleMessage(eventArgs.Message, eventArgs.MsgID, eventArgs.EventConnection);
            }
            catch (ConcourseException ex)
            {
                Logger.LogErrorMessage(ex);
                string msg = string.Format("Error occured while handling message (ID:{0}) from connection {1}", eventArgs.MsgID, eventArgs.EventConnection != null ? eventArgs.EventConnection.Name : "Unknown");
                Logger.LogErrorMessage(msg);                
            }
        }

        #endregion

    }
}
