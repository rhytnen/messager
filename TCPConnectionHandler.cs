﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Threading;

using ConcourseCommon;

namespace ConcourseNet
{
    public class TCPConnectionHandler : Connection
    {
        #region fields

        TcpClient client = null;
        Dictionary<int, MessageCallBack> callbacks = new Dictionary<int, MessageCallBack>();
        Queue<MessageContainer> messages = new Queue<MessageContainer>();
        Dictionary<int, Dictionary<int, List<MessageFragment>>> FragmentStore = null;
        byte[] buffer = new byte[MessageFragment.FragmentHeaderSize];
        string address = string.Empty;
        int port = 0;
        string localHost = string.Empty;
        string remoteHost = string.Empty;
        readonly object writeLock = new object();
        Thread receiveThread = null;
        bool isShutdown = false;

        #endregion

        #region properties

        public string Address
        {
            get { return address; }
        }

        public string LocalHost
        {
            get { return localHost; }
        }

        public string RemoteHost
        {
            get { return remoteHost; }
        }

        public IPEndPoint LocalEndPoint
        {
            get
            {
                IPEndPoint endPoint = null;
                Socket socket = client.Client;
                if (socket != null)
                {
                    endPoint = (IPEndPoint)socket.LocalEndPoint;
                }
                return endPoint;
            }
        }

        public IPEndPoint RemoteEndPoint
        {
            get
            {
                IPEndPoint endPoint = null;
                Socket socket = client.Client;
                if (socket != null)
                {
                    endPoint = (IPEndPoint)socket.RemoteEndPoint;
                }
                return endPoint;
            }
        }

        public bool Connected
        {
            get 
            { 
                if (client == null) 
                    return false;
                else
                    return client.Connected; 
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor that initiates a connection to some host / port via tcp.
        /// </summary>
        /// <param name="hostname">The host to connect with.</param>
        /// <param name="port">The port to connect on.</param>
        public TCPConnectionHandler(string hostname, int port)
        {
            remoteHost = hostname;
            this.port = port;
            name = string.Format("{0}:{1}", hostname, port);

            //check if hostname is actually IP address
            try
            {
                IPAddress.Parse(hostname);
                address = hostname;
            }
            catch (FormatException) { }


            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry(hostname);
                remoteHost = hostEntry.HostName;
                localHost = Dns.GetHostName();
            }
            catch (SocketException) { }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Construtor that initializes a new instance of TCPConnectionHandler using a already existing TcpClient
        /// </summary>
        /// <param name="tcpclient">Existing TCPClient connection</param>
        /// <exception cref="ConcourseNetException"></exception>
        public TCPConnectionHandler(TcpClient tcpclient)
        {
            if (tcpclient.Connected == false)
            {
                string msg = string.Format("Connection to {0}:{1} not established. TCPConnectionHandler initialization failed.", address, port);
                throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_failed);
            }

            client = tcpclient;
            Socket socket = client.Client;
            if (socket != null)
            {
                IPEndPoint endPoint = (IPEndPoint)socket.RemoteEndPoint;
                if (endPoint != null)
                {
                    address = endPoint.Address.ToString();
                    port = endPoint.Port;
                    name = string.Format("{0}:{1}", address, port);
                }
            }

            try
            {
                remoteHost = Dns.GetHostEntry(address).HostName;
                localHost = Dns.GetHostName();
            }
            catch (SocketException) { }
            catch (ArgumentException) { }

            isShutdown = false;
            FragmentStore = new Dictionary<int, Dictionary<int, List<MessageFragment>>>();

            receiveThread = new Thread(new ParameterizedThreadStart(ReceiveThread));
            receiveThread.Name = string.Format("TCPConnectionHandler.ReceiveThread for {0}:{1}", address, port);
            receiveThread.IsBackground = true;
            receiveThread.Start(null);
        }

        #endregion

        #region methods for Connect/Disconnect

        public void Disconnect()
        {
            isShutdown = true;
            if (client != null)
            {
                client.Close();
            }
            callbacks.Clear();
            if (receiveThread != null)
            {
                receiveThread.Join();
            }
        }

        /// <summary>
        /// Establishes connection to specified host and port
        /// </summary>
        /// <exception cref="ConcourseNetException"></exception>
        public void Connect()
        {
            try 
            {
                isShutdown = false;
                client = new TcpClient(remoteHost, port);
                FragmentStore = new Dictionary<int, Dictionary<int, List<MessageFragment>>>();

                if (address.Length <= 0)
                {
                    Socket socket = client.Client;
                    if (socket != null)
                    {
                        IPEndPoint endPoint = (IPEndPoint)socket.RemoteEndPoint;
                        if (endPoint != null)
                        {
                            address = endPoint.Address.ToString();
                        }
                    }
                }

                receiveThread = new Thread(new ParameterizedThreadStart(ReceiveThread));
                receiveThread.Name = string.Format("TCPConnectionHandler.ReceiveThread for {0}:{1}", address, port);
                receiveThread.IsBackground = true;
                receiveThread.Start(null);
            }
            catch (SocketException ex)
            {
                string msg = string.Format("Attempt to establish connection to {0}:{1} failed.", address, port);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_failed);
            }
        }

        /// <summary>
        /// An overriden function that closes the tcp connection.
        /// </summary>
        override public void Close()
        {
            Disconnect();
        }

        /// <summary>
        /// Returns the connection status of this connection.
        /// </summary>
        /// <returns>True if connected, false otherwise.</returns>
        override public  bool IsConnected()
        {
            if (client == null)
            {
                return false;
            }
            else
            {
                return client.Connected;
            }
        }

        #endregion

        #region methods for receiving and handling msgs

        private void ReceiveThread(object data)
        {
            MessageFragment fragment = null;

            while (true)
            {
                if (isShutdown)
                    break;

                try
                {
                    fragment = null;
                    ReceiveMsgFragment(ref fragment);
                }
                catch (ConcourseNetException ex)
                {
                    if (!isShutdown)
                    {
                        Logger.LogErrorMessage(ex);
                        Logger.LogErrorMessage(string.Format("Attempt to receive data from {0}:{1} failed", address, port));
                    }

                    return;
                }

                try
                {
                    if (fragment.SeqTotal == 1)
                    {
                        MessageContainer msg = new MessageContainer();
                        if (fragment.Length > 0)
                            msg.Message = fragment.GetData();
                        msg.Id = (int)fragment.msgId;
                        HandleMessage(msg);
                        continue;
                    }

                    if (!FragmentStore.ContainsKey(fragment.ThreadID))
                    {
                        FragmentStore[fragment.ThreadID] = new Dictionary<int, List<MessageFragment>>();
                    }
                    if (!FragmentStore[fragment.ThreadID].ContainsKey(fragment.HashID))
                    {
                        FragmentStore[fragment.ThreadID][fragment.HashID] = new List<MessageFragment>();
                    }

                    FragmentStore[fragment.ThreadID][fragment.HashID].Add(fragment);
                    if (fragment.Complete == true) 
                    {
                        MessageFragment[] frags = FragmentStore[fragment.ThreadID][fragment.HashID].ToArray();
                        FragmentStore[fragment.ThreadID].Remove(fragment.HashID);
                        if (FragmentStore[fragment.ThreadID].Count == 0)
                            FragmentStore.Remove(fragment.ThreadID);
                        MessageContainer msg = MessageFragment.AssembleFragments(frags);
                        HandleMessage(msg);
                        continue;
                    }
                }
                catch (ConcourseException ex)
                {
                    if (!isShutdown)
                    {
                        Logger.LogErrorMessage(ex);
                        Logger.LogErrorMessage(string.Format("Processing received data from {0}:{1} failed, MsgID: {2}", address, port, fragment.msgId));
                    }
                }
            }
        }

        void ReceiveMsgFragment(ref MessageFragment fragment)
        {
            NetworkStream stream = client != null ? client.GetStream() : null;
            if (stream == null)
            {
                string msg = string.Format("Unable to receive data from {0}:{1} because missing network stream", address, port);
                throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_disconnected);
            }

            if (fragment == null)
            {
                fragment = new MessageFragment();
            }

            int numBytesReceived = 0, numBytesRead = 0;

            //first receive the header bytes
            while (numBytesReceived < MessageFragment.FragmentHeaderSize)
            {
                if (isShutdown)
                {
                    string msg = string.Format("Unable to receive data from {0}:{1} because the connection is aborting", address, port);
                    throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_aborted);
                }

                try
                {
                    numBytesRead = stream.Read(buffer, numBytesReceived, MessageFragment.FragmentHeaderSize - numBytesReceived);
                    if (numBytesRead <= 0)
                    {
                        string msg = string.Format("Unable to receive data from {0}:{1} because the connection is disconnected.", address, port);
                        throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_disconnected);                    
                    }
                    numBytesReceived += numBytesRead;
                }
                catch (IOException ex)
                {                  
                    string msg = string.Format("Attempt to receive data from {0}:{1} failed.", address, port);
                    throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_disconnected);
                }
            }

            fragment.ParseHeader(buffer);
            if (fragment.Length <= 0)
            {
                return;
            }

            //Now receive the data bytes
            byte[] data = new byte[fragment.Length];
            numBytesReceived = 0;
            while (numBytesReceived < fragment.Length)
            {
                if (isShutdown)
                {
                    string msg = string.Format("Unable to receive data from {0}:{1} because the connection is aborting", address, port);
                    throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_aborted);
                }

                try
                {
                    numBytesRead = stream.Read(data, numBytesReceived, fragment.Length - numBytesReceived);
                    if (numBytesRead <= 0)
                    {
                        string msg = string.Format("Unable to receive data from {0}:{1} because the connection is disconnected.", address, port);
                        throw new ConcourseNetException(msg, ConcourseNetError.ConcourseNet_connection_disconnected);                        
                    }
                    numBytesReceived += numBytesRead;
                }
                catch (IOException ex)
                {
                    string msg = string.Format("Attempt to receive data from {0}:{1} failed.", address, port);
                    throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_disconnected);
                }
            }

            fragment.SetData(data);
        }

        /// <summary>
        /// When a message is received it notfies the appropriate callback methods.
        /// </summary>
        /// <param name="msg"></param>
        void HandleMessage(MessageContainer msg)
        {
            int id = msg.Id;
            byte[] message = msg.Message;

            if (callbacks.ContainsKey(id))
            {
                callbacks[id](id, message, this);
            }

            OnMsgReceived(id, message);
        }

        #endregion

        #region methods for sending msgs
 
        /// <summary>
        ///  Write a message on a particular id.
        /// </summary>
        /// <param name="id">The id to write the message on.</param>
        /// <param name="message">The message to write.</param>
        /// <exception cref="ConcourseNetException"></exception>
        override public void WriteMessage(int id, byte[] message)
        {
            try
            {
                MessageFragment[] fragments = MessageFragment.FragmentMessage(id, message);
                NetworkStream n = client.GetStream();
                for (int j = 0; j < fragments.Length; j++)
                {
                    byte[] data = fragments[j].GetBytes();
                    lock (writeLock)
                    {
                        n.Write(data, 0, data.Length);
                    }
                }
            }
            catch (SystemException ex)
            {
                string msg = string.Format("Attempt to send data on connection {0}:{1} failed.", address, port);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_data_send_failed);
            }
        }

        /// <summary>
        /// A writeMessage that automaticaly serializes and sends your object.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="o"></param>
        /// <exception cref="ConcourseNetException"></exception>
        public void WriteObject(int id, Object o)
        {
            try
            {
                BinaryFormatter binf = new BinaryFormatter();
                MemoryStream stream = new MemoryStream();
                binf.Serialize(stream, o);
                byte[] m = stream.GetBuffer();
                WriteMessage(id, m);
            }
            catch (SystemException ex)
            {
                string msg = string.Format("Attempt to send data on connection {0}:{1} failed.", address, port);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_data_send_failed);
            }
        }

        /// <summary>
        /// Implementation to allow non-message messages.  this will fake a message currently but
        /// will eventually support null messages.
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="ConcourseNetException"></exception>
        public void WriteMessage(int id)
        {
            byte[] m = new byte[1];
            WriteMessage(id, m);
        }

        #endregion

        #region methods for registering/deregistering for msgs

        /// <summary>
        /// The definition of the delegate function that is called for a specific message.
        /// </summary>
        /// <param name="id">The id that was registered.</param>
        /// <param name="message">The message received on that id.</param>
        public delegate void MessageCallBack(int id, byte[] message, TCPConnectionHandler handler);

        /// <summary>
        /// For a particular id, specify a MessageCallBack function that will handle the message that come in on the id.
        /// </summary>
        /// <param name="id">The id to register.</param>
        /// <param name="mcb">The function to handle messages on id.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void RegisterForMessage(int id, MessageCallBack mcb)
        {
            if (callbacks.ContainsKey(id))
                return;
            callbacks.Add(id, mcb);
            byte[] message = System.BitConverter.GetBytes(id);
            WriteMessage(0, message);
        }

        /// <summary>
        /// Remove a id from the list of messages that is handled.
        /// </summary>
        /// <param name="id">The id to remove from the list.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void DeregisterFormessage(int id)
        {
            callbacks.Remove(id);
            byte[] message = System.BitConverter.GetBytes(id);
            WriteMessage(-1, message);
        }

        #endregion

        /// <summary>
        /// A connection uses this function send a system level message to identify itself.
        /// </summary>
        /// <param name="name">The name by which this connection is to be known.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void SetName(string name)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] msg = encoding.GetBytes(name);
            WriteMessage(-2, msg);
        }
    }
}
