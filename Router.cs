﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using ConcourseNet;
using System.Diagnostics;

using ConcourseCommon;

namespace ConcourseNet
{
    public class Router
    {
        public ConnectionManager conManager;  // applications 
        public int port = 0;
        public int udpPort = 0;

        /// <summary>
        /// Router just owns a listener and recieves input.
        /// The connection manager is doing the work of message passing.
        /// </summary>
        /// <param name="args"></param>
        /// <exception cref="ConcourseNetException"></exception>
        public Router(int Port)
        {
            conManager = new ConnectionManager();            
            port = Port;
            udpPort = port + 1;
            /* Open up the connection to accept router connections before notifying them you 
             * are alive or accepting alive requests.
             */
            try
            {
                TcpListener listener = new TcpListener(IPAddress.Any, port);
                listener.Start();
                listener.BeginAcceptTcpClient(MakeClientConnection, listener);
            }
            catch (SocketException ex)
            {
                string msg = string.Format("Attempt to listen for connection on {0} failed.", port);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNe_listening_connection_failed);
            }
        }

        /// <summary>
        /// Turn on the UDP Listener on a specified port.
        /// </summary>
        /// <param name="uport">Port to listen on.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void UDPListen(int uport)
        {
            udpPort = uport;
            UdpClient udpc;
            try
            {
                IPEndPoint ipepAny = new IPEndPoint(IPAddress.Any, udpPort);
                udpc = new UdpClient(ipepAny);
                udpc.BeginReceive(MakeUDPConnection, udpc);
            }
            catch (SocketException ex)
            {
                string msg = string.Format("Attempt to receive data on UDP port {0} failed.", udpPort);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_failed);
            }
        }

        /// <summary>
        /// Send "PING" on a specific port and address via UDP.
        /// </summary>
        /// <param name="address">IPAddress to send to.</param>
        /// <param name="uport">Port to send on.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void UDPPing(IPAddress address, int uport)
        {
            UDPWrite("PING", address, uport);
        }

        /// <summary>
        /// Send "ACK" on a specific port and address via UDP.
        /// </summary>
        /// <param name="address">IPAddress to send to.</param>
        /// <param name="uport">Port to send on.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void UDPACK(IPAddress address, int uport)
        {
            UDPWrite("ACK", address, uport);
        }

        /// <summary>
        /// Write a given message to some port / address via UDP.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="address">IPAddress to send to.</param>
        /// <param name="uport">Port to send on.</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void UDPWrite(string message, IPAddress address, int uport)
        {
            try
            {
                UdpClient cli = new UdpClient();
                IPEndPoint ipep = new IPEndPoint(address, uport);
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                byte[] bytestring = encoding.GetBytes(message);
                cli.Send(bytestring, bytestring.Length, ipep);
            }
            catch (SocketException ex)
            {
                string msg = string.Format("Attempt to send data on UDP connection {0}:{1} failed.", address, uport);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_data_send_failed);
            }
        }

        /// <summary>
        /// UDP broadcast a message on a particular port.  This translates into
        /// multicast on ff02::1 for ipv6.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="uport"></param>
        /// <exception cref="ConcourseNetException"></exception>
        public void UDPBroadcast(string message, int uport)
        {
            /* apparently the broadcast on 255.255.255.255 is antiquated and is only used for
             * dhcp discovery.  The preferred method is to send to the broadcast of each interface
             * even though it is clumsy to do so.  So that's what this loop is for.  */
            IPHostEntry iphe = Dns.GetHostEntry(Environment.MachineName);
            foreach (IPAddress ipa in iphe.AddressList)
            {
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                byte[] bytestring = encoding.GetBytes(message);
                IPEndPoint ipepBC = null;
                try
                {
                    if (ipa.AddressFamily == AddressFamily.InterNetwork)
                    {
                        string addy = ipa.ToString();
                        int loc = addy.LastIndexOf(".");
                        string bcast = addy.Substring(0, loc) + ".255";
                        ipepBC = new IPEndPoint(IPAddress.Parse(bcast), uport);
                        UdpClient cli = new UdpClient();
                        cli.Send(bytestring, bytestring.Length, ipepBC);

                    }
                    else if (ipa.AddressFamily == AddressFamily.InterNetworkV6)
                    {
                        Socket s1 = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp);
                        s1.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                        ipepBC = new IPEndPoint(IPAddress.Parse("ff02::1"), uport);
                        s1.SetSocketOption(SocketOptionLevel.IPv6,
                        SocketOptionName.AddMembership, new IPv6MulticastOption(IPAddress.Parse("ff02::1")));
                        s1.SendTo(bytestring, ipepBC);
                    }
                }
                catch (SocketException ex)
                {
                    string msg = string.Format("Attempt to broadcast message on {0} failed.", (ipepBC != null ? ipepBC.ToString() : ""));
                    throw new ConcourseNetException(msg, ex, ConcourseNetError.ConcourseNet_connection_data_send_failed);
                }
            }  
        }

        public void CloseAll()
        {
            conManager.CloseConnections();
        }

        /// <summary>
         /// This will add a new connection to the list of connections in conManager
         /// they automatically listen for new data at this point and no further 
         /// action is needed.  The connection manager should start forwarding messages
         /// on to things that declare their interest
         /// </summary>
         /// <param name="ar">standard async state variable</param>
        public void MakeClientConnection (IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;
            TcpClient c = listener.EndAcceptTcpClient(ar);

            try
            {
                TCPConnectionHandler con = new TCPConnectionHandler(c);
                conManager += con;                
            }
            catch (ConcourseNetException ex)
            {   //since this function runs on thread pool, log an error instead of throwing new exception
                Logger.LogErrorMessage(ex);
            }

            try
            {
                listener.BeginAcceptTcpClient(MakeClientConnection, listener);
            }
            catch (SocketException ex)
            {   //since this function runs on thread pool, log an error instead of throwing new exception
                Logger.LogErrorMessage(ex);
                string msg = string.Format("Attempt to listen for connection on {0} failed.", listener.LocalEndpoint.ToString());
                Logger.LogErrorMessage(msg);
            }
        }

        /// <summary>
        /// Asynchronous listen for ANNOUNCE_ALIVE from other routers and make a tcp connection to them.
        /// </summary>
        /// <param name="ar">standard async state variable</param>
        public void MakeUDPConnection(IAsyncResult ar)
        {
            UdpClient udpc = (UdpClient)ar.AsyncState;
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 0);
                byte [] msg = udpc.EndReceive(ar, ref ipep);                
                string message = System.Text.ASCIIEncoding.UTF8.GetString(msg);

                if (message.Equals("PING"))
                {
                    UDPACK(ipep.Address, udpPort + 1);
                }
                else if (message.Equals("CON"))
                {
                    IPAddress[] ips = Dns.GetHostAddresses(Environment.MachineName);
                    for (int i = 0; i < ips.Length; i++)
                    {
                        if (ips[i].Equals(ipep.Address)) // ignore myself 
                        {
                            udpc.BeginReceive(MakeUDPConnection, udpc);
                            return;
                        }
                    }

                    TcpClient tcpc = new TcpClient();
                    ipep.Port = port;
                    tcpc.Connect(ipep);
                    TCPConnectionHandler tcpConnect = new TCPConnectionHandler(tcpc);
                    tcpConnect.IsHub = true;
                    conManager += tcpConnect;

                    // tell the new router what we are interested in.
                    byte[] m = new byte[sizeof(int)];
                    m = System.BitConverter.GetBytes(-3);
                    tcpConnect.WriteMessage(-3, m);

                    foreach (int id in conManager.RegisteredIds)
                    {
                        m = System.BitConverter.GetBytes(id);
                        tcpConnect.WriteMessage(0, m);
                    }
                }
            }
            catch (ConcourseNetException ex)
            {   //since this function runs on thread pool, log an error instead of throwing new exception
                Logger.LogErrorMessage(ex);
            }
            catch (SocketException ex)
            {   //since this function runs on thread pool, log an error instead of throwing new exception
                Logger.LogErrorMessage(ex);
            }

            udpc.BeginReceive(MakeUDPConnection, udpc);
            return;
        }
    }

}
