﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ConcourseCommon;

namespace ConcourseNet
{
    #region Aggregation
    /// <summary>
    /// An aggregated collection of frame data
    /// derived from EventArgs so that it can also be used as parameter for aggregation events
    /// </summary>
    public class Aggregation : EventArgs
    {
        #region fields

        NanoNetFlagTypes type = NanoNetFlagTypes.NANO_FLAG_NONE;
        uint id = 0;
        string path = string.Empty;
        byte[] data = null;
        int idx = 0;  // also would be the accumulated length
        uint totalLength = 0;

        #endregion

        #region properties

        public NanoNetFlagTypes Type
        {
            get { return type; }
            set { type = value; }
        }

        public uint ID
        {
            get { return id; }
            set { id = value; }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        public uint TotalLength
        {
            get { return totalLength; }
            set { totalLength = value; }
        }

        public byte[] Data
        {
            get { return data; }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the Aggregation class with the specified size for data
        /// </summary>
        /// <param name="length">Expected length of the data.</param>
        public Aggregation(uint length)
        {
            data = new byte[length];
            totalLength = length;
        }

        /// <summary>
        /// Adds a chunk of data to the current Aggregation.
        /// </summary>
        /// <param name="buffer">The data to append</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void AppendData(byte[] buffer)
        {
            AppendData(buffer, 0, buffer.Length);
        }
        
        /// <summary>
        /// Adds a chunk of data to the current Aggregation starting at specified index and specified length
        /// </summary>
        /// <param name="buffer">The data to append</param>
        /// <param name="beginIndex">Starting index in buffer</param>
        /// <param name="dataSize">Amount of data to append</param>
        /// <exception cref="ConcourseNetException"></exception>
        public void AppendData(byte[] buffer, int beginIndex, int dataSize)
        {
            try
            {
                Array.Copy(buffer, beginIndex, data, idx, dataSize);
                idx += dataSize;
            }
            catch (ArgumentException ex)
            {
                throw new ConcourseNetException("Attempt to add data to NanoNetFrame aggregation failed", ex, ConcourseNetError.Aggregating_NanoNetFrame_data_failed);
            }
        }

        /// <summary>
        /// Query an aggregation to see if it is complete.
        /// </summary>
        /// <returns>true if complete, false otherwise.</returns>
        public bool AggregationComplete()
        {
            return (idx == totalLength);
        }
    }
    #endregion

    #region FrameAggregator
    /// <summary>
    /// Class recieves frame and aggregates their data buffer.
    /// Another class can register to recieve the data for a specific flag type
    /// </summary>
    public class NanoNetFrameAggregator
    {
 
        #region fields

        Dictionary<UInt32, Aggregation> Aggregations = new Dictionary<UInt32, Aggregation>();   //key is TaskID

        #endregion

        #region events

        public event EventHandler<Aggregation> AggregationCompleteEvent;

        #endregion

        /// <summary>
        /// Initializes a new instance of the NanoNetFrameAggregator
        /// </summary>
        public NanoNetFrameAggregator()
        {
        }

        /// <summary>
        /// HandleNanoNetFrame() takes a frame and stores it by task id
        /// until the amount of data it's received matches the amount of
        /// data the frame says should be accumulated.  It then calls an
        /// event to pass the aggregation to interested parties.
        /// </summary>
        /// <param name="frame">A NanoNetFrame that contains some data to aggregate with other frames.</param>
        /// <returns></returns>
        /// <exception cref="ConcourseNetException"></exception>
        public void HandleNanoNetFrame(NanoNetFrame frame)
        {
            if (frame == null || frame.Data == null)
            {
                throw new ConcourseNetException("Attempt to aggregate data from nanonet frame failed", ConcourseNetError.Missing_NanoNetFrame_data);
            }

            UInt32 taskid = (UInt32)frame.TaskID;
            int dataBeginIndex = 0;
            int dataSize = frame.DataSize;

            if (frame.Num == 1)  // first frame received
            {   // add the aggregate data
                Aggregation agg = new Aggregation(frame.Param1);
                agg.Type = (NanoNetFlagTypes)frame.FlagID;

                if (agg.Type == NanoNetFlagTypes.NANO_FLAG_SEND_MEMBLOCK_DATA)
                {   //first 4 byte of data contains the ID
                    agg.ID = BitConverter.ToUInt32(frame.Data, 0);
                    dataBeginIndex = 4;
                    dataSize -= 4;
                }
                else if (agg.Type == NanoNetFlagTypes.NANO_FLAG_SEND_FILE_DATA || agg.Type == NanoNetFlagTypes.NANO_FLAG_SEND_DIRECTORY_DATA)
                {   //the null terminated ascii file path is at the begining of the data buffer
                    int firstNullIndex = dataSize-1;
                    for (int i=0; i<dataSize; ++i)
                    {
                        if (frame.Data[i] == 0)
                        {
                            firstNullIndex = i;
                            break;
                        }
                    }
                    agg.Path = Encoding.UTF8.GetString(frame.Data, 0, firstNullIndex);
                    dataBeginIndex = firstNullIndex + 1;
                    dataSize -= firstNullIndex + 1;
                }

                Aggregations[taskid] = agg;
            }
            else if (Aggregations.ContainsKey(taskid) == false)
            {
                string msg = string.Format("Attempt to aggregate data from nanonet frame ({0}) failed.", frame.ToString());
                throw new ConcourseNetException(msg, ConcourseNetError.Missing_aggregation_in_NanonetAggregator);
            }

            try
            {
                Aggregations[taskid].AppendData(frame.Data, dataBeginIndex, dataSize);

                if (Aggregations[taskid].AggregationComplete())
                {
                    if (AggregationCompleteEvent != null)
                    {
                        AggregationCompleteEvent(this, Aggregations[taskid]);
                    }

                    Aggregations.Remove(taskid); // no longer needed                   
                }
            }
            catch (ConcourseException e)
            {
                Aggregations.Remove(taskid);
                string msg = string.Format("Attempt to aggregate data from nanonet frame ({0}) failed.", frame.ToString());
                throw new ConcourseNetException(msg, e, ConcourseNetError.Aggregating_NanoNetFrame_data_failed);
            }

            return;
        }

        /// <summary>
        /// Removes aggregation for the specified task ID
        /// </summary>
        /// <param name="taskID">Task ID to remove</param>
        public void RemoveData(uint taskID)
        {
            Aggregations.Remove(taskID);
        }

    }
    #endregion
}
