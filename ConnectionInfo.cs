﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace ConcourseNet
{
    [Serializable]
    public class ConnectionInfo
    {
        #region fields

        string name = string.Empty;
        string ipAddress = string.Empty;
        int port = 0;
        DateTime connectTime = DateTime.MinValue;

        #endregion

        #region properties

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string IPAddress
        {
            get { return ipAddress; }
            set 
            {
                if (value.Length == 0 ||
                    value == null)
                    throw new ArgumentException("Invalid IP Address");
                System.Net.IPAddress add;
                bool ret = System.Net.IPAddress.TryParse(value, out add);
                if (ret == false)
                    throw new ArgumentException("Invalid IP Address");
                ipAddress = value;
            }
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        public DateTime ConnectTime
        {
            get { return connectTime; }
            set { connectTime = value; }
        }

        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the ConnectionInfo
        /// </summary>
        public ConnectionInfo()
        {

        }

        /// <summary>
        ///  Initializes a new instance of the ConnectionInfo with specified name, IPAdress and port
        /// </summary>
        /// <param name="name">Name of the connection</param>
        /// <param name="ipAddress">IPAddress of the connection</param>
        /// <param name="port">Port number of the connection</param>
        public ConnectionInfo(string name, string ipAddress, int port)
        {
            this.name = name;
            this.ipAddress = ipAddress;
            this.port = port;

        }

        #endregion

        #region methods for XML serializing/deserializing

        /// <summary>
        /// Loads a list of ConnectionInfo object from specified XML file
        /// </summary>
        /// <param name="filePath">Path of XML file</param>
        /// <returns>List of loaded ConnectionInfo objects</returns>
        /// <exception cref="ConcourseNetException"></exception>
        public static List<ConnectionInfo> LoadConnectionList(string filePath)
        {
            try
            {
                using (Stream fStream = File.OpenRead(filePath))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<ConnectionInfo>), new Type[] { typeof(ConnectionInfo) });
                    return (List<ConnectionInfo>)serializer.Deserialize(fStream);
                }
            }
            catch (ArgumentException ex)
            {
                string msg = string.Format("Attempt to load connection list from {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_load_failed);
            }
            catch (IOException ex)
            {
                string msg = string.Format("Attempt to load connection list from {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_load_failed);
            }
            catch (InvalidOperationException ex)
            {
                string msg = string.Format("Attempt to load connection list from {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_load_failed);
            }
        }

        /// <summary>
        /// Saves a list of ConnectionInfo objects in XML format
        /// </summary>
        /// <param name="connectionList">List of ConnectionInfo objects</param>
        /// <param name="filePath">Path of XML file</param>
        /// <exception cref="ConcourseNetException"></exception>
        public static void SaveConnectionList(List<ConnectionInfo> connectionList, string filePath)
        {
            try
            {
                using (Stream fStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<ConnectionInfo>), new Type[] { typeof(ConnectionInfo) });
                    serializer.Serialize(fStream, connectionList);
                }
            }
            catch (ArgumentException ex)
            {
                string msg = string.Format("Attempt to save connection list to {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_save_failed);
            }
            catch (IOException ex)
            {
                string msg = string.Format("Attempt to save connection list to {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_save_failed);
            }
            catch (InvalidOperationException ex)
            {
                string msg = string.Format("Attempt to save connection list to {0} failed", filePath);
                throw new ConcourseNetException(msg, ex, ConcourseNetError.ConnectionInfo_list_save_failed);
            }
        }

        #endregion
    }
}
