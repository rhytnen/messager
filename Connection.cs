﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConcourseNet
{
    /// <summary>
    /// An abstract concept of a connection that represents a source of some data.  Implementations may include TCP, UDP,
    /// shared memory, file io and so on.
    /// </summary>
    abstract public class Connection
    {

        #region fields

        protected string name = string.Empty;
        int connectionID = 0;
        protected bool isHub = false;

        static int connectionIDCounter = 0;
        static object counterLock = new object();

        #endregion

        #region properties

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int ConnectionID
        {
            get { return connectionID; }
        }

        public bool IsHub
        {
            get { return isHub; }
            set { isHub = value; }
        }

        #endregion

        #region events

        public event EventHandler<ConnectionEventArgs> MessageReceivedEvent;

        #endregion

        #region constructor

        /// <summary>
        /// Standard constructor.  it sets the default name to "unknown".
        /// </summary>
        public Connection()
        {
            name = "unknown";
            AssignConnectionID();
        }

        void AssignConnectionID()
        {
            lock (counterLock)
            {
                if (connectionIDCounter == int.MaxValue)
                    connectionIDCounter = 0;
                connectionID = ++connectionIDCounter;
            }
        }

        #endregion

        #region abstract methods

        /// <summary>
        /// Close a this particular connection.
        /// </summary>
        abstract public void Close();

        /// <summary>
        /// Write a message on a particular id.
        /// </summary>
        /// <param name="message">The message to write.</param>
        /// <param name="id">The id to write the message on.</param>
        /// <exception cref="ConcourseNetException"></exception>
        abstract public void WriteMessage(int id, byte[] m);

        /// <summary>
        /// Returns the connection status of a connection.
        /// </summary>
        /// <returns>True on connected, false otherwise.</returns>
        abstract public bool IsConnected();

        #endregion

        protected void OnMsgReceived(int id, byte[] msg)
        {
            if (MessageReceivedEvent != null)
            {
                ConnectionEventArgs connectionEventArgs = new ConnectionEventArgs(id, msg, this);
                MessageReceivedEvent(this, connectionEventArgs);
            }
        }
    }
    
}
