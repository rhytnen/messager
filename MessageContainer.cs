using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Timers;

namespace ConcourseNet
{
    public class MessageFragment
    {
        #region member variables
        public const int FragmentSize = 4096;
        public const int FragmentHeaderSize = (2 * sizeof(int) + 4 * sizeof(UInt32) + sizeof(bool));
        int threadId;
        int hashId;
        int messageId;
        int length;
        int totalLength;
        int seqNumber;
        int seqTotal;
        bool complete;
        byte[] payload = null;
        #endregion

        #region properties
        public int TotalLength
        {
            get { return totalLength; }
            set { totalLength = value; }
        }

        public int msgId
        {
            get { return messageId; }
            set { messageId = value; }
        }

        public int Length
        {
            get 
            {
                //if (payload != null) 
                //    return payload.Length;
                //return 0;
                return length;
            }
            set { length = value; }
        }

        public int SeqNumber
        {
            get { return seqNumber; }
            set { seqNumber = value; }
        }

        public int SeqTotal
        {
            get { return seqTotal; }
            set { seqTotal = value; }
        }
        public int HashID
        {
            get { return hashId; }
            set { hashId = value; }
        }

        public int ThreadID
        {
            get { return threadId; }
            set { threadId = value; }
        }

        public bool Complete
        {
            get { return complete; }
            set { complete = value; }
        }

        public void SetData(byte[] data, int offset, int length)
        {
            totalLength = length;
            payload = new byte[length];
            Array.Copy(data, offset, payload, 0, length);
            //HashData();
        }

        public void SetData(byte[] data)
        {
            totalLength = data.Length;
            payload = data;
            //HashData();
        }

        public byte[] GetData()
        {
            return payload;
        }
        #endregion

        #region constructors
        public MessageFragment(byte[] buffer)
        {
            ParseBytes(buffer);
        }

        public MessageFragment()
        {
            return;
        }
        #endregion

        public void HashData()
        {
            int hash = 0;
            int len = Math.Min(payload.Length, 10);
            for (int i = 0; i < len; i++)
            {
                hash = (hash << 5) + hash + payload[i];
            }
            hashId = hash;
        }

        /// <summary>
        /// Prints the key data about a fragment.
        /// </summary>
        public void PrintFragment()
        {
            Console.WriteLine("Fragment:");
            Console.WriteLine("ThreadID: {0}", threadId);
            Console.WriteLine("Message ID: {0}", messageId);
            Console.WriteLine("HashCode: {0}", hashId);
            Console.WriteLine("SeqTotal: {0}", seqTotal);
            Console.WriteLine("SeqNumber: {0}", seqNumber);
            Console.WriteLine("Length: {0}", length);
            Console.WriteLine("Complete: {0}", complete);
        }
        /// <summary>
        /// Returns a byte array that contains necessary properties for reconstructing the fragment.
        /// Typical used to distribute the information across the network. 
        /// </summary>
        /// <returns>A byte array containing the payload and properties of the fragment.  The order of alignment is:
        ///          ThreadID: 4 bytes
        ///          HashID: 4 bytes
        ///          SeqNumber: 4 bytes
        ///          SeqTotal: 4 bytes
        ///          Complete: 1 byte
        ///          MessageID: 4 bytes
        ///          Payload Length: 4 bytes
        ///          Payload in byte[]
        /// </returns>
        /// 
        public byte[] GetBytes()
        {
            int bufsize = (FragmentHeaderSize + payload.Length);
            byte[] buf = new byte[bufsize];

            Array.Copy(BitConverter.GetBytes(threadId), buf, 4);
            Array.Copy(BitConverter.GetBytes(hashId), 0, buf, 4, 4);
            Array.Copy(BitConverter.GetBytes(seqNumber), 0, buf, 8, 4);
            Array.Copy(BitConverter.GetBytes(seqTotal), 0, buf, 12, 4);
            Array.Copy(BitConverter.GetBytes(complete), 0, buf, 16, 1);
            Array.Copy(BitConverter.GetBytes(messageId), 0, buf, 17, 4);
            Array.Copy(BitConverter.GetBytes(payload.Length), 0, buf, 21, 4);
            Array.Copy(payload, 0, buf, 25, payload.Length);
            return buf;
        }



        /// <summary>
        /// Given a byte[], this function will populate the variables for the message fragment.
        /// </summary>
        /// <param name="buffer"></param>
        public void ParseBytes(byte[] buffer)
        {
            threadId = BitConverter.ToInt32(buffer, 0);
            hashId = BitConverter.ToInt32(buffer, 4);
            seqNumber = BitConverter.ToInt32(buffer, 8);
            seqTotal = BitConverter.ToInt32(buffer, 12);
            complete = BitConverter.ToBoolean(buffer, 16);
            messageId = BitConverter.ToInt32(buffer, 17);
            length = BitConverter.ToInt32(buffer, 21);
            payload = new byte[length];
            Array.Copy(buffer, 25, payload, 0, length);

        }

        /// <summary>
        /// Given a buffer, it reads through it and fills key data with appropriate values.
        /// </summary>
        /// <param name="buffer"></param>
        public void ParseHeader(byte[] buffer)
        {
            threadId = BitConverter.ToInt32(buffer, 0);
            hashId = BitConverter.ToInt32(buffer, 4);
            seqNumber = BitConverter.ToInt32(buffer, 8);
            seqTotal = BitConverter.ToInt32(buffer, 12);
            complete = BitConverter.ToBoolean(buffer, 16);
            messageId = BitConverter.ToInt32(buffer, 17);
            length = BitConverter.ToInt32(buffer, 21);
        }
        /// <summary>
        ///  Given a data stream, this function will return an array of message fragments suitable for 
        ///  sending to some receiver that also knows about fragments.
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static MessageFragment[] FragmentMessage(int messageId, byte[] data)
        {
            int numFragments = (int)((float)data.Length / FragmentSize);
            if ( data.Length > (numFragments * FragmentSize) ) 
                numFragments++;
            int hash = 0;
            int len = Math.Min(data.Length, 10);
            for (int i = 0; i < len; i++)
            {
                hash = (hash << 5) + hash + data[i];
            }

            MessageFragment[] fragments = new MessageFragment[numFragments];
            int offset = 0;
            for (int i = 0; i < numFragments; i++)
            {
                int length = FragmentSize;
                if (i == numFragments - 1) 
                    length = data.Length - offset;

                MessageFragment fm = new MessageFragment();
                fm.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
                fm.hashId = hash;
                fm.Length = length;
                fm.msgId = messageId;
                fm.SeqNumber = i + 1;
                fm.SeqTotal = numFragments;
                fm.Complete = false;
                if (i == numFragments - 1)
                    fm.Complete = true;
                fm.totalLength = data.Length;
                fm.SetData(data, offset, length);
                offset+=length;
                
                fragments[i] = fm;

            }
            return fragments;

        }

        public static MessageFragment MakeEmptyFragment(int messageId)
        {
            MessageFragment fm = new MessageFragment();
            fm.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
            fm.hashId = 0;
            fm.Length = 0;
            fm.msgId = messageId;
            fm.SeqNumber = 1;
            fm.SeqTotal = 1;
            fm.Complete = true;
            fm.totalLength = 0;

            return fm;
        }
        /// <summary>
        /// Given a series of MessageFraments, this function will assemble them into the orginal byte array.
        /// </summary>
        /// <param name="fragments"></param>
        /// <returns></returns>
        public static MessageContainer AssembleFragments(MessageFragment[] fragments)
        {
            try
            {
                byte[] data;
                int size = 0;
                for (int i = 0; i < fragments.Length; i++)
                {
                    size += fragments[i].Length;
                }

                data = new byte[size];

                int offset = 0;
                for (int i = 0; i < fragments.Length; i++)
                {
                    Array.Copy(fragments[i].GetData(), 0, data, offset, fragments[i].length);
                    offset += (int)fragments[i].Length;
                }

                MessageContainer msg = new MessageContainer();
                msg.Message = data;
                msg.Id = (int)fragments[0].msgId;
                return msg;
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} {1}", e.Message, e.StackTrace);
                return null;
            }
        }

        
    }

    /// <summary>
    /// A class containing key data regarding a fully formed message
    /// </summary>
    public class MessageContainer
    {
        Int32 length;
        Int32 id;
        byte[] header = new byte[2 * sizeof(UInt32)];
        byte[] message;

        /// <summary>
        /// A const header size.
        /// </summary>
        public int HeaderSize
        {
            get { return header.Length; }
        }

        /// <summary>
        /// The length of the data part of the message.
        /// </summary>
        public Int32 Length
        {
            get
            {
                if (message == null)
                    return 0;
                return message.Length;
            }
            set
            {
                length = value;
                message = new byte[length];
            }
        }

        /// <summary>
        /// The message id the message was sent on.
        /// </summary>
        public Int32 Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// The header part of the message.
        /// </summary>
        public byte[] Header
        {
            get { return header; }
            set
            {
                header = value;
                length = BitConverter.ToInt32(header, 0);
                id = BitConverter.ToInt32(header, 4);
                message = new byte[length];
            }
        }

        /// <summary>
        /// Length of the data part of the message.
        /// </summary>
        public byte[] Message
        {
            get { return message; }
            set 
            { 
                message = value;
                length = message.Length;     
            }
        }


        /// <summary>
        /// Read header buffer and populate key data.
        /// </summary>
        public void ParseHeader()
        {
            length = BitConverter.ToInt32(header, 0);
            id = BitConverter.ToInt32(header, 4);
            message = new byte[length];
        }

        
    }
}
